FROM python:3
# Set environment variables
#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1
WORKDIR /app
RUN pip install virtualenv
RUN virtualenv venv
RUN /bin/bash -c "source venv/bin/activate"
COPY . .
RUN pip install -r requirements.txt
CMD [ "python","manage.py","runserver" ]